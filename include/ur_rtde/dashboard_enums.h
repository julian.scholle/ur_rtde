#ifndef RTDE_DASHBOARD_ENUMS_H
#define RTDE_DASHBOARD_ENUMS_H
#include <iostream>

namespace ur_rtde
{
class PolyScopeVersion
{
 public:
  PolyScopeVersion(const std::string &str)
  {
    parse(str);
  }

  int major;
  int minor;
  int patch;
  int build;

  std::string toString();
  void parse(const std::string &str);
};

enum class ProgramState
{
  STOPPED,
  PLAYING,
  PAUSED
};

ProgramState parseProgramState(const std::string &state_str);
std::string toString(const ProgramState &mode);

enum class UserRole
{
  PROGRAMMER,
  OPERATOR,
  NONE,
  LOCKED,
  RESTRICTED
};
enum class SafetyMode
{
  NORMAL = 1,
  REDUCED = 2,
  PROTECTIVE_STOP = 3,
  RECOVERY = 4,
  SAFEGUARD_STOP = 5,
  SYSTEM_EMERGENCY_STOP = 6,
  ROBOT_EMERGENCY_STOP = 7,
  VIOLATION = 8,
  FAULT = 9,
  VALIDATE_JOINT_ID = 10,
  UNDEFINED_SAFETY_MODE = 11
};

SafetyMode parseSafetyMode(const std::string &state_str);
std::string toString(const SafetyMode &mode);

enum class SafetyStatus
{
  NORMAL,
  REDUCED,
  PROTECTIVE_STOP,
  RECOVERY,
  SAFEGUARD_STOP,
  SYSTEM_EMERGENCY_STOP,
  ROBOT_EMERGENCY_STOP,
  VIOLATION,
  FAULT,
  AUTOMATIC_MODE_SAFEGUARD_STOP,
  SYSTEM_THREE_POSITION_ENABLING_STOP
};

SafetyStatus parseSafetyStatus(const std::string &state_str);
std::string toString(const SafetyStatus &mode);

enum class RobotMode
{
  NO_CONTROLLER = -1,
  DISCONNECTED = 0,
  CONFIRM_SAFETY = 1,
  BOOTING = 2,
  POWER_OFF = 3,
  POWER_ON = 4,
  IDLE = 5,
  BACKDRIVE = 6,
  RUNNING = 7,
  UPDATING_FIRMWARE = 8
};

RobotMode parseRobotMode(const std::string &state_str);
std::string toString(const RobotMode &mode);

}  // namespace ur_rtde

#endif  // RTDE_DASHBOARD_ENUMS_H