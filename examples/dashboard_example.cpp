#include <ur_rtde/dashboard_client.h>
#include <ur_rtde/dashboard_enums.h>
#include <ur_rtde/rtde.h>
#include <ur_rtde/rtde_control_interface.h>
#include <ur_rtde/rtde_receive_interface.h>
#include <cmath>
#include <iostream>

using namespace ur_rtde;

void receive_callback(RTDEReceiveInterface * rtde_receive){
  std::cout<<"test"<<std::endl;
}


int main()
{
  //std::string ur_host = "10.42.0.20";
  std::string ur_host = "localhost";

  RTDEControlInterface rtde_control(ur_host);
  RTDEReceiveInterface rtde_receive(ur_host);
  rtde_receive.registerCallback(std::bind(receive_callback, &rtde_receive));

  auto dashboardClient = rtde_control.getDashboardClient();

  //  std::cout << "------------------------------\nTest: "<< "PolyscopeVersion" << std::endl;
  //  PolyScopeVersion test(dashboardClient->PolyscopeVersion());
  //  std::cout << "PolyScopeVersion: " << test.toString() << std::endl;
  //
  //  std::cout << "------------------------------\nTest: "<< "programState" << std::endl;
  //  std::cout << dashboardClient->programState() << std::endl;
  //  std::cout << "programState: " << toString(parseProgramState(dashboardClient->programState())) << std::endl;
  //
  //  std::cout << "------------------------------\nTest: "<< "robotmode" << std::endl;
  //  std::cout << dashboardClient->robotmode() << std::endl;
  //  std::cout << "Robotmode: " << toString(parseRobotMode(dashboardClient->robotmode())) << std::endl;
  //
  //  std::cout << "------------------------------\nTest: "<< "safetystatus" << std::endl;
  //  std::cout << dashboardClient->safetystatus() << std::endl;
  //  std::cout << "Safetystatus: " << toString(parseSafetyStatus(dashboardClient->safetystatus())) << std::endl;
  //
  //  std::cout << "------------------------------\nTest: "<< "safetymode" << std::endl;
  //  std::cout << dashboardClient->safetymode() << std::endl;
  //  std::cout << "Safetymode: " << toString(parseSafetyMode(dashboardClient->safetymode())) << std::endl;
  //
  //  //std::cout << "------------------------------\nTest: "<< "setUserRole" << std::endl;
  //  //dashboardClient->setUserRole(UserRole::PROGRAMMER);
  //
  //  std::cout << "------------------------------\nTest: "<< "isProgramSaved" << std::endl;
  //  std::cout << "isProgramSaved: " << (dashboardClient->isProgramSaved() ? "True" : "False" ) << std::endl;

  // std::cout << "------------------------------\nTest: "<< "unlockProtectiveStop" << std::endl;
  // dashboardClient->unlockProtectiveStop();

  // std::cout << "------------------------------\nTest: "<< "restartSafety" << std::endl;
  // dashboardClient->restartSafety();
  dashboardClient->closeSafetyPopup();
  dashboardClient->closePopup();
  while (true)
  {
    auto mode = parseRobotMode(dashboardClient->robotmode());
    auto status = parseSafetyStatus(dashboardClient->safetystatus());
    std::cout << "Robot Mode is: " << toString(mode) << std::endl;
    std::cout << "Robot State is: " << toString(status) << std::endl;
    switch (status)
    {
      case SafetyStatus::NORMAL:
        break;
      case SafetyStatus::PROTECTIVE_STOP:
      {
        std::cout << "unlockProtectiveStop" << std::endl;
        dashboardClient->unlockProtectiveStop();
        break;
      }
      case SafetyStatus::ROBOT_EMERGENCY_STOP:
      case SafetyStatus::FAULT:
      {
        std::cout << "restart safety" << std::endl;
        dashboardClient->restartSafety();
        std::cout << "waiting for reboot" << std::endl;
        do
        {
          dashboardClient->closeSafetyPopup();
          dashboardClient->closePopup();
          mode = parseRobotMode(dashboardClient->robotmode());
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
        } while (mode != RobotMode::BOOTING);
      }
      default:
        break;
    }

    switch (mode)
    {
      case RobotMode::POWER_OFF:
        std::cout << "Powering on Robot!" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        dashboardClient->powerOn();
        break;

      case RobotMode::IDLE:
        std::cout << "Releasing Breaks!" << std::endl;
        dashboardClient->brakeRelease();
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        while (!rtde_control.teachMode())
        {
          std::cout << "reconnect!" << std::endl;
          rtde_control.endTeachMode();
          rtde_control.reuploadScript();
        }
        dashboardClient->closePopup();
        break;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }

  return 0;
}