#include <ur_rtde/dashboard_client.h>
#include <ur_rtde/dashboard_enums.h>
#include <ur_rtde/rtde.h>
#include <ur_rtde/rtde_control_interface.h>
#include <ur_rtde/rtde_receive_interface.h>
#include <cmath>
#include <iostream>

using namespace ur_rtde;
int main()
{
  RTDEReceiveInterface rtde_receive("127.0.0.1");

  while (1)
  {
    std::cout << "Actual q is: " << std::endl;
    for (const auto &d : rtde_receive.getActualQd())
      std::cout << d << " ";
    std::cout << std::endl;
  }
  return 0;
}